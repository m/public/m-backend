# MBackendWorkspace

This project was generated using [Nx](https://nx.dev). It contains a list of packages used in the M organization's server side built with nodeJS.

## Packages

- M Cli: a package that helps build backend projects with a fairly stable webpack version. [Check the documentation here](packages/cli/README.md)

- Core: a boilerplate code for M servers built with nodeJS. [Check the documentation here](packages/core/README.md)

## Generate a library

Run `nx g @nx/workspace:library my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well. Or via the VSCode Nx Console extension.

Libraries are shareable across libraries and applications. They can be imported from `@m-backend-workspace/mylib`.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.
