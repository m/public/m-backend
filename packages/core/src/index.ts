import 'reflect-metadata';
import { constructor } from 'tsyringe/dist/typings/types';
import { container } from 'tsyringe';
import { program } from 'commander';
import { LoggerService } from './lib/logger.service';
import { ConsoleBase } from './lib/console/console.abstract';
import { CommandSpec, CONSOLE_META } from './lib/decorators/console.decorator';
import { ModuleConfig, MODULE_META } from './lib/decorators/module.decorator';
import { CORE_APPLICATIONS, CORE_CONSOLES, CORE_CONTROLLERS, CORE_CRONS, CORE_MIDDLEWARES } from './lib/core.token';

export interface SystemConfig {
  consoles: constructor<any>[],
  applications: constructor<any>[],
  controllers: constructor<any>[],
  middlewares: constructor<any>[],
  crons: constructor<any>[]
}

export interface OptionalSystemConfig {
  token: constructor<any> | string,
  config: constructor<any>[]
}

export const systemConfigs: SystemConfig = {
  consoles: [],
  applications: [],
  controllers: [],
  middlewares: [],
  crons: []
};


export function registerModule(module: constructor<any>, currentContainer?: any): void {
  const config: ModuleConfig = Reflect.getMetadata(MODULE_META.CONFIG, module);
  // Do nothing if the module do nothing.
  if (!config) {
    return;
  }
  let _container = container.createChildContainer();
  if (currentContainer) {
    _container = currentContainer;
  }
  if (config.imports) {
    config.imports.forEach((importedModule) => {
      registerModule(importedModule, _container);
    });
  }

  Object.entries(systemConfigs).forEach(([key, data]) => {
    if (key in config) {
      (config as any)[key].forEach((target: constructor<any>) => {
        data.push(target);
      });
    }
  });
}

export function bootstrap(mainModule: constructor<any>, optionalSystemConfigs?: OptionalSystemConfig[]) {

  registerModule(mainModule);
  container.register(CORE_CONSOLES, { useValue: systemConfigs.consoles });
  container.register(CORE_APPLICATIONS, { useValue: systemConfigs.applications });
  container.register(CORE_CONTROLLERS, { useValue: systemConfigs.controllers });
  container.register(CORE_MIDDLEWARES, { useValue: systemConfigs.middlewares });
  container.register(CORE_CRONS, { useValue: systemConfigs.crons });
  if (optionalSystemConfigs && optionalSystemConfigs.length > 0) {
    optionalSystemConfigs.forEach((optionalSystemConfig: OptionalSystemConfig) => {
      container.register(optionalSystemConfig.token, { useValue: optionalSystemConfig.config });
    });
  }

  const log = container.resolve(LoggerService);

  process.on('uncaughtException', (err, origin) => {
    log.logger.error(`[uncaughtException][origin] ${origin}`);
    log.logger.error(`[uncaughtException][error] ${err}`);
    log.logger.error(`[uncaughtException][stack] ${err.stack}`);
  });

  process.on('unhandledRejection', (reason: PromiseRejectionEvent, p) => {
    log.logger.error(`[unhandledRejection][reason] Unhandled Rejection at: Promise ${p} / Reason: ${reason}`);
  });

  // Setup the command line commands and options.
  let exit = false;
  Object.entries(systemConfigs.consoles).forEach(([filename, wrapper]) => {
    const instance = container.resolve<ConsoleBase>(wrapper);
    log.logger.debug(`[console][${instance.name}] registered.`);
    let commands: CommandSpec[] = Reflect.getMetadata(CONSOLE_META.COMMANDS, wrapper);
    commands = commands.filter(c => instance instanceof c.target.constructor);
    if (commands.length === 0) {
      exit = true;
      log.logger.warn(`[console][${instance.name}] no commands found.`);
    }
    commands.forEach(commandSpec => {
      const command = program.command(`${instance.name}:${commandSpec.command}`, commandSpec.commandOptions);
      command.description(commandSpec.description);
      commandSpec.options.forEach(optionSpec => {
        command.option(optionSpec.option, optionSpec.description, optionSpec.default);
      });
      command.action(async (...args) => {
        exit = !!commandSpec.exit;
        log.logger.silly(`[console][${instance.name}:${(commandSpec.propertyKey as string)}] Execute command.`);
        return await (instance as any)[commandSpec.propertyKey](...args);
      });
      log.logger.debug(`[console][${instance.name}:${(commandSpec.propertyKey as string)}] registered.`);
    });
  });

  program.parseAsync(process.argv).then(() => {
    // Exit after executing a command.
    if (exit) {
      process.exit();
    }
  });
}


export * from './lib/applications';
export * from './lib/console';
export * from './lib/decorators';
export * from './lib/config';
export * from './lib/core.module';
export * from './lib/core.token';
export * from './lib/cron.interface';
export * from './lib/fake._middleware';
export * from './lib/logger.service';
export * from './lib/middleware.interface';
