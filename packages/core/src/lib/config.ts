/**
 * Allow user to load and get config files.
 * /!\ Need config files to be externals => See the webpack/webpack.config.prod.js
 */

import { writeFileSync, existsSync, readdir } from "fs";
import path from 'path';

/**
 * Config object contains each config file data loaded using the loadConfigFile function.
 */
export const config: { [key: string]: any } = {};

// Configuration file that allows to create whitelist and blacklist
const mBackendConfigPath = process.cwd().concat('\\m-backend.json');

export async function loadConfigFile(name: string, content?: string) {
  // If configuration already contains name, skip loadConfigFile
  /* if (name in config) return; */

  // Get absolute path and file path
  const currentDirectory = process.cwd();
  let configFilePath = `${currentDirectory}/config/${name}.config.json`;

  // If mBackend file is set, check for defined extensions, else keep default .config.json extension
  if (existsSync(mBackendConfigPath)) {
    const mBackendConfig = require(mBackendConfigPath);

    // Check if extension is managed in configuration, else skip the file
    if (mBackendConfig.whitelist.some((extension: string) => name.includes(extension))) {
        configFilePath = `${currentDirectory}/config/${name}`;
    }
  }

  if (existsSync(configFilePath)) {
    // Import into config list the location of the existing configuration file
    config[name] = require(configFilePath);
  } else if (content) {
    // Create the new configuration file
    writeFileSync(configFilePath, content);
  }
}

export async function loadConfigFolder(directoryPath?: string) {
  // Logger labels
  const BASE = '[Config][loadConfigFolder]';
  const DIRECTORY = BASE + '[DirectoryPath]';
  const CONFIGURATION = BASE + '[MbackendConfigPath]';
  const BLACKLIST = BASE + '[BlacklistFile]';
  const WHITELIST = BASE + '[WhitelistFile]';
  const SKIPPED = BASE + '[SkippedFile]';

  // Define configuration file location from function parameter or default location
  directoryPath = directoryPath || path.join(process.cwd(), 'config');

  console.log(DIRECTORY, directoryPath);
  console.log(CONFIGURATION, mBackendConfigPath);

  // IndexedFiles is used to log which file is manage by which function
  const indexedFiles: string[] = [];

  // Read the content of the configuration folder
  readdir(directoryPath, (err, files) => {

    // Handling error if the folder can't be read
    if (err) return console.log('Unable to scan directory : ' + err);

       // Listing all files in the folder
       files.forEach((file) => {

        // If the path to m-backend.json exists
        if (existsSync(mBackendConfigPath)) {

            // Import m-backend.json
            const mBackendConfig = require(mBackendConfigPath);

            // If a blacklist is set in m-backend.json
            if (mBackendConfig && mBackendConfig.blacklist) {
                // Check if file or extension match blacklist
                if (mBackendConfig.blacklist.some((extension: string) => file.includes(extension))) {
                    console.log(BLACKLIST, file);
                    indexedFiles.push(file);
                    return;
                }
            }

            // If a whitelist configuration is defined
            if (mBackendConfig && mBackendConfig.whitelist) {
                // Check if file or extension match whitelist
                if (mBackendConfig.whitelist.some((extension: string) => file.includes(extension))) {
                    console.log(WHITELIST, file);
                    indexedFiles.push(file);
                    loadConfigFile(file);
                    return;
                }
            } else {
                // If whitelist option doesn't exists in m-backend.json index all files that are not blacklisted
                if (!indexedFiles.includes(file)) {
                    console.log(WHITELIST, file);
                    indexedFiles.push(file);
                    loadConfigFile(file);
                    return;
                }
            }

            // If file is ignored by whitelist and blacklist
            if (!indexedFiles.includes(file)) {
                console.log(SKIPPED, file);
            }

        } else {
            // If no configuration file has been defined simply test .config.json extension
            if(file.includes('.config.json')) {
              console.log(WHITELIST, file);
              loadConfigFile(file);
            }
            else console.log(SKIPPED, file);
        }
    });
  });
}


