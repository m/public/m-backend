import { Context, Next } from "koa";

export interface MiddlewareInterface {

	use(ctx: Context, next: Next): any;

}
