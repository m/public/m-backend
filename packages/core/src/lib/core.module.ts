import { Module } from "./decorators/module.decorator";
// import MainApp from "./applications/main.app";
import AppConsole from "./console/app.console";

@Module({
	// applications: [MainApp],
	consoles: [AppConsole],
})
export class CoreModule { }
