import * as winston from 'winston';
import DailyRotateFile, { DailyRotateFileTransportOptions } from 'winston-daily-rotate-file';
import { config } from "./config";
import { Service } from "./decorators/service.decorator";
import { ConsoleTransportOptions, FileTransportOptions } from 'winston/lib/winston/transports';
import LokiTransport from 'winston-loki';

//options type not exported... 
type LokiTransportOptions = ConstructorParameters<typeof LokiTransport>[0];

export interface LoggerConfig {
  level: string;
  transports: { 
    type: 'file' | 'daily-rotate-file' | 'loki' | 'console-no-color' | 'console', 
    options: FileTransportOptions | ConsoleTransportOptions | DailyRotateFileTransportOptions | LokiTransportOptions 
  }[];
}

@Service()
export class LoggerService {

  config: LoggerConfig;
  logger: winston.Logger;
  colorizer = winston.format.colorize();
  profiler: winston.Profiler | undefined;

  constructor () {
    this.config = config.app.logger;
    this.logger = this.buildLogger();
  }

  private buildLogger(): winston.Logger {
    const _options: winston.LoggerOptions = {
      level: this.config.level || 'info',
      format: winston.format.combine(
        winston.format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.json()
      ),
      transports: []
    };
    if (this.config.transports) {
      this.config.transports.forEach(({ type, options }) => {
        switch (type) {
          case 'console':
            if (_options.transports) {
              (_options.transports as any[]).push(new winston.transports.Console({
                ...options as ConsoleTransportOptions,
                format: winston.format.combine(
                  winston.format.printf(info => {
                    return `${info.timestamp} [${this.colorize(info.level, info.level)}]${info.message}`;
                  }),
                )
              }));
            }
            break;
          case 'file':
            if (_options.transports) {
              (_options.transports as any[]).push(new winston.transports.File({
                ...options as FileTransportOptions,
                format: winston.format.combine(
                  winston.format.printf(info => {
                    return `${info.timestamp} [${info.level}] ${info.message}`;
                  }),
                )
              }));
            }
            break;
          case 'daily-rotate-file':
            if (_options.transports) {
              (_options.transports as any[]).push(new DailyRotateFile({
                ...options as DailyRotateFileTransportOptions,
                format: winston.format.combine(
                  winston.format.printf(info => {
                    return `${info.timestamp} [${info.level}] ${info.message}`;
                  }),
                )
              }));
            }
            break;
          case 'loki':
              if (_options.transports) {
                (_options.transports as any[]).push(new LokiTransport({
                  ...options as LokiTransportOptions,
                  format: winston.format.combine(
                    winston.format.printf(info => {
                      return `${info.timestamp} [${info.level}] ${info.message}`;
                    }),
                  )
                }));
              }
              break;
          case 'console-no-color':
            if (_options.transports) {
              (_options.transports as any[]).push(new winston.transports.Console({
                ...options as ConsoleTransportOptions,
                format: winston.format.combine(
                  winston.format.printf(info => {
                    return `${info.timestamp} [${info.level}] ${info.message}`;
                  }),
                )
              }));
            }
            break;
        }
      });
    }

    return winston.createLogger(_options);
  }

  colorize(level: string, message: string): string {
    return this.colorizer.colorize(level, message);
  }

  startTimer() {
    this.profiler = this.logger.startTimer();
  }

  stopTimer() {
    if (this.profiler) {
      this.profiler.done({
        message: 'Worker job done.'
      });
    }
  }
}
