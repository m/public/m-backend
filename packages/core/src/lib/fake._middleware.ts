import { MiddlewareInterface } from "./middleware.interface";
import { Middleware } from "./decorators/middleware.decorator";
import { Context, Next } from "koa";

@Middleware()
export default class FakeMiddleware implements MiddlewareInterface {
	async use(ctx: Context, next: Next) {
		await next();
	}

}
