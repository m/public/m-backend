
export interface OnTick {
	onTick(): void;
}

export interface OnComplete {
	onComplete(): void;
}

export interface OnInit {
	onInit(): void;
}
