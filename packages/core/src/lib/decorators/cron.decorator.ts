import { constructor } from 'tsyringe/dist/typings/types';
import { CronJob, CronJobParams } from 'cron';
import { singleton } from 'tsyringe';

export interface PartialCronJobParameters extends Omit<CronJobParams, 'onTick' | 'onComplete'> { }

export enum CRON_META {
	OPTIONS = 'cron:options'
}

export function Cron<T>(options: PartialCronJobParameters): (target: constructor<T>) => void {
	return (target: constructor<T>) => {
		singleton()(target);
		Reflect.defineMetadata(CRON_META.OPTIONS, options, target);
	};
}
