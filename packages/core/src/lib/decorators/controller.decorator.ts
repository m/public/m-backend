import { constructor } from "tsyringe/dist/typings/types";
import { singleton } from "tsyringe";
import { OutputValidation, Spec } from "@koa-better-modules/joi-router";
import { BusboyConfig } from '@fastify/busboy';
import { SchemaLike, ValidationOptions } from "joi";
import { Options } from "co-body";

export interface ValidateOption {
	header?: SchemaLike | undefined;
	query?: SchemaLike | undefined;
	params?: SchemaLike | undefined;
	body?: SchemaLike | undefined;
	maxBody?: number | string | undefined;
	failure?: number | undefined;
	type?: 'form' | 'json' | 'multipart' | undefined;
	formOptions?: Options | undefined;
	jsonOptions?: Options | undefined;
  multipartOptions?: BusboyConfig | undefined;
	output?: { [status: string]: OutputValidation } | undefined;
	continueOnError?: boolean | undefined;
	validateOptions?: ValidationOptions | undefined;

}

export enum CONTROLLER_META {
	APP = 'controller:app',
	PREFIX = 'controller:prefix',
	ROUTES = 'controller:routes',
	MIDDLEWARE = 'controller:middleware'
}

/**
 * Controller decorator, allow dependency injection.
 * @param options
 */
export function Controller<T>(options?: { prefix?: string, app?: string }): (target: constructor<T>) => void {
	const _options = { prefix: '', app: 'main', ...options || {} };
	return function (target: constructor<T>): void {
		// Allow DI
		singleton()(target);
		// Controller Specifics
		Reflect.defineMetadata(CONTROLLER_META.APP, _options.app, target);
		Reflect.defineMetadata(CONTROLLER_META.PREFIX, _options.prefix, target);
	}
}

/**
 * Router request factory, helpers and decorators.
 */

function findRouteSpec(propertyKey: string, list: Spec[]): Spec | undefined {
	return list.find(r => r.meta && r.meta.propertyKey === propertyKey);
}

function findOrCreateRouteSpec(propertyKey: string, list: Spec[]): Spec {
	let routeSpec = findRouteSpec(propertyKey, list);
	if (!routeSpec) {
		routeSpec = {
			path: '',
			method: [],
			handler: () => { },
			meta: {
				propertyKey: propertyKey,
				middlewares: []
			}
		};
		list.push(routeSpec);
	}
	return routeSpec;
}

/**
 *
 * @param method the method name ('POST', 'PUT', 'GET', 'DELETE').
 * @param path the route path.
 * @param validate the validate object according to the joi koa router.
 */
function requestFactory(method: string, path: string, validate?: ValidateOption, meta?: any): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
	return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
		const routes: Spec[] = Reflect.getMetadata(CONTROLLER_META.ROUTES, target) || [];
		const routeSpec: Spec = findOrCreateRouteSpec(propertyKey, routes);
		routeSpec.path = path;
		(routeSpec.method as string[]).push(method);
		if (validate) {
			routeSpec.validate = validate;
		}
    if (meta) {
      routeSpec.meta = { ...routeSpec.meta , ...meta};
    }

		Reflect.defineMetadata(CONTROLLER_META.ROUTES, routes, target);
	}
}

export function Get(path: string, validate?: ValidateOption, meta?: any): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
	return requestFactory('GET', path, validate, meta);
}

export function Post(path: string, validate?: ValidateOption, meta?: any): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
	return requestFactory('POST', path, validate, meta);
}

export function Put(path: string, validate?: ValidateOption, meta?: any): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
	return requestFactory('PUT', path, validate, meta);
}

export function Delete(path: string, validate?: ValidateOption, meta?: any): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
	return requestFactory('DELETE', path, validate, meta);
}

/**
 * Middleware decorator.
 * @param middlewares A middleware list.
 */
export function AttachMiddleware(middlewares: any[] = []): (target: any, propertyKey?: string) => void {
	return (target: any, propertyKey?: string) => {
		// Case: method decorator.
		if (propertyKey) {
			const routes: Spec[] = Reflect.getMetadata(CONTROLLER_META.ROUTES, target) || [];
			const routeSpec: Spec = findOrCreateRouteSpec(propertyKey, routes);
			routeSpec.meta.middlewares = middlewares;
			Reflect.defineMetadata(CONTROLLER_META.ROUTES, routes, target);
		} else {
			// Case: class decorator.
			Reflect.defineMetadata(CONTROLLER_META.MIDDLEWARE, middlewares, target);
		}
	}
}
