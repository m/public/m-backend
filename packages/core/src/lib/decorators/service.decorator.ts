import { constructor } from "tsyringe/dist/typings/types";
import { singleton } from "tsyringe";

/**
 * Service decorator, allow dependency injection.
 */
export function Service<T>(): (target: constructor<T>) => void {
	return function (target: constructor<T>): void {
		// Allow DI
		singleton()(target);
	}
}
