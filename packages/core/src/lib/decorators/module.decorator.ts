import { constructor } from 'tsyringe/dist/typings/types';

export enum MODULE_META {
	CONFIG = 'module:config'
}

export interface ModuleConfig {
	imports?: constructor<any>[];
	applications?: constructor<any>[];
	controllers?: constructor<any>[];
	consoles?: constructor<any>[];
	middlewares?: constructor<any>[];
	crons?: constructor<any>[];
	providers?: any[];
	entities?: any[];
}

export function Module(config: ModuleConfig): (target: constructor<any>) => void {
	return (target: constructor<any>) => {
		Reflect.defineMetadata(MODULE_META.CONFIG, config, target);
	};
}
