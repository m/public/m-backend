import { constructor } from "tsyringe/dist/typings/types";
import { singleton, scoped, Lifecycle } from "tsyringe";

export enum MIDDLEWARE_META {
	AUTOLOAD_IN = 'middleware:autoloadIn'
}

/**
 * Middleware decorator, allow dependency injection.
 * @param options The parameter autoload must be a valid application name (eg: 'main'). There is automatically load by the app.
 */
export function Middleware<T>(options?: { autoloadIn?: string, scoped?: boolean }): (target: constructor<T>) => void {
	return function (target: constructor<T>): void {
		// Allow DI
		if (!options || (options && !options.scoped)) {
			singleton()(target);
		} else {
			scoped(Lifecycle.ResolutionScoped)(target);
		}
		// Middleware specific
		if (options && options.autoloadIn) {
			Reflect.defineMetadata(MIDDLEWARE_META.AUTOLOAD_IN, options.autoloadIn, target);
		}
	}
}
