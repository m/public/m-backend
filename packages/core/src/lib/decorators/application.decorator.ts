import { constructor } from "tsyringe/dist/typings/types";
import { singleton } from "tsyringe";
import { AbstractApp } from "../applications";

export const appContainer: { [key: string]: constructor<AbstractApp> } = {};

export function Application<T>(name: string, port: number, httpOptions?: { httpsOptions?: any, http2Options?: any }): (target: constructor<T>) => void {
	return function (target: constructor<T>): void {
		// Allow DI
		singleton()(target);
		// Application Specifics
		target.prototype.name = name;
		target.prototype.port = port;
		target.prototype.httpOptions = httpOptions;
		appContainer[name] = target as any;
	}
}
