import { constructor } from "tsyringe/dist/typings/types";
import { singleton } from "tsyringe";

export enum CONSOLE_META {
	COMMANDS = 'console:commands'
}

export function Console<T>(name: string): (target: constructor<T>) => void {
	return function (target: constructor<T>): void {
		// Allow DI
		singleton()(target);
		// Console Specifics
		target.prototype.name = name;
	}
}

export interface CommandSpec {
	command: string;
	description: string;
	propertyKey: string | symbol;
	options: OptionSpec[];
	commandOptions: any;
	exit?: boolean;
	target: any;
}

export interface OptionSpec {
	option: string;
	description: string;
	default?: any;
}

export function Command(command: string, description: string, options: OptionSpec[] = [], commandOptions?: any): MethodDecorator {
	return function (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>): void {
		const commands: CommandSpec[] = Reflect.getMetadata(CONSOLE_META.COMMANDS, target.constructor) || [];
		const column = commands.find(c => c.propertyKey === propertyKey);
		if (!column) {
			commands.push({ command, description, propertyKey, options, commandOptions, exit: true, target });
		}
		Reflect.defineMetadata(CONSOLE_META.COMMANDS, commands, target.constructor);
	}
}

export function NonExitCommand(command: string, description: string, options: OptionSpec[] = [], commandOptions?: any): MethodDecorator {
	return function (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>): void {
		const commands: CommandSpec[] = Reflect.getMetadata(CONSOLE_META.COMMANDS, target.constructor) || [];
		const column = commands.find(c => c.propertyKey === propertyKey);
		if (!column) {
			commands.push({ command, description, propertyKey, options, commandOptions, target });
		}
		Reflect.defineMetadata(CONSOLE_META.COMMANDS, commands, target.constructor);
	}
}
