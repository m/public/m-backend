import Koa from 'koa';
import { MIDDLEWARE_META } from '../decorators';
import { LoggerService } from '../logger.service';
import { MiddlewareInterface } from '../middleware.interface';
import https from 'https';
import http2 from 'http2';

export abstract class AbstractApp {
	public instance: Koa;
	// Name and port properties are injected during the decorator 'application' function call.
	public name: string | undefined;
	public port: string | undefined;
	public httpOptions: { httpsOptions?: any, http2Options?: any } | undefined;

	constructor(public log: LoggerService, middlewares: MiddlewareInterface[]) {
		this.instance = new Koa();
		middlewares.forEach(middleware => {
			const autoloadIn = Reflect.getMetadata(MIDDLEWARE_META.AUTOLOAD_IN, middleware.constructor);
			if (autoloadIn === this.name) {
				this.instance.use(async (ctx, next) => {
					return await middleware.use(ctx, next);
				});
			}
		});

	}

	public listen() {
		const callback = this.instance.callback();
		if (this.httpOptions && this.httpOptions.httpsOptions) {
			const httpsServer = https.createServer(this.httpOptions.httpsOptions, callback);
			httpsServer.listen(this.port, () => this.log.logger.info(`[application][${this.name}] listening on https://localhost:${this.port}`));
		} else if(this.httpOptions && this.httpOptions.http2Options) {
			const http2Server = http2.createServer(this.httpOptions.http2Options, callback);
			http2Server.listen(this.port, () => this.log.logger.info(`[application][${this.name}] listening in http2 on https://localhost:${this.port} `));
		} else {
			this.instance.listen(this.port, () => this.log.logger.info(`[application][${this.name}] listening on http://localhost:${this.port}`));
		}
		
	}
}
