import { AbstractApp } from "../applications";
import { Application } from "../decorators/application.decorator";
import { config, loadConfigFile } from "../config";
import { inject, injectAll } from "tsyringe";
import { LoggerService } from "../logger.service";
import { MiddlewareInterface } from "../middleware.interface";

loadConfigFile('app');

@Application('main', config.app.port)
export default class MainApp extends AbstractApp {

	constructor(@inject(LoggerService) logger: LoggerService, @injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = []) {
		super(logger, middlewares);
	}

}
