export const CORE_APPLICATIONS = 'core:applications';
export const CORE_CONTROLLERS = 'core:controllers';
export const CORE_MIDDLEWARES = 'core:middlewares';
export const CORE_CRONS = 'core:crons';
export const CORE_CONSOLES = 'core:consoles';
