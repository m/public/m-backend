import { inject, container } from "tsyringe";
import { constructor } from "tsyringe/dist/typings/types";
import Router, { Handler } from "@koa-better-modules/joi-router";
const KoaJoiRouter = require('@koa-better-modules/joi-router');
import { CronJob } from "cron";
import { ConsoleBase } from "./console.abstract";
import { AbstractApp } from "../applications";
import { CORE_APPLICATIONS, CORE_MIDDLEWARES, CORE_CONTROLLERS, CORE_CRONS } from "../core.token";
import { Console, NonExitCommand, CONTROLLER_META, appContainer, CRON_META } from "../decorators";
import { LoggerService } from "../logger.service";
import { MiddlewareInterface } from "../middleware.interface";
import { Context } from "koa";

@Console('app')
export default class AppConsole extends ConsoleBase {

	constructor(
		@inject(CORE_APPLICATIONS) private applications: constructor<AbstractApp>[],
		@inject(CORE_MIDDLEWARES) private middlewares: constructor<MiddlewareInterface>[],
		@inject(CORE_CONTROLLERS) private controllers: constructor<any>[],
		@inject(CORE_CRONS) private crons: constructor<any>[],
		private log: LoggerService
	) { super() }

	@NonExitCommand('start', 'Start the application with web features', [], { isDefault: true })
	start() {
		// The @injectAll decorator doesn't support empty array,
		// so we populate the middleware array with a fake middleware that do nothing.
		if (this.middlewares.length === 0) {
			this.middlewares.push(require('../fake._middleware').default);
		}

		// Register middlewares with the MiddlewareInterface injection token to allow the use of @injectAll().
		this.middlewares.forEach((middlewareClass) => {
			container.register('MiddlewareInterface', { useClass: middlewareClass });
			this.log.logger.debug(`[middleware][${middlewareClass.name}] registered.`);
		});

		/**
		 * Configure routing
		 */
		this.controllers.forEach((controller) => {
			const instance = container.resolve(controller);
			const prefix = Reflect.getMetadata(CONTROLLER_META.PREFIX, controller);
			const routes = Reflect.getMetadata(CONTROLLER_META.ROUTES, instance) as Router.Spec[] || [];
			const routerMiddlewares = Reflect.getMetadata(CONTROLLER_META.MIDDLEWARE, controller) as constructor<MiddlewareInterface>[] || [];
			const app = container.resolve<AbstractApp>(appContainer[Reflect.getMetadata(CONTROLLER_META.APP, controller)]);
      		const _router = new KoaJoiRouter();
			routerMiddlewares.forEach(middlewareClass => {
				const middlewareInstance = container.resolve(middlewareClass);
				_router.use(async (ctx:Context, next:any) => {
					return await middlewareInstance.use(ctx, next);
				});
			})
			_router.prefix(prefix);
			routes.forEach((route: Router.Spec) => {
				const handlers: Handler[] = [];
				route.meta.middlewares.forEach((middleware: constructor<MiddlewareInterface>) => {
					const middlewareInstance = container.resolve(middleware);
					handlers.push(async (ctx, next) => {
						return await middlewareInstance.use(ctx, next);
					})
				});
				handlers.push(async (...args) => {
					await instance[route.meta.propertyKey](...args);
				});
				route.handler = handlers;
				_router.route(route);
				this.log.logger.debug(`[controller][${controller.name}] ${prefix + route.path}`);
			});
			app.instance.use(_router.middleware());
		});

		/**
		 * Applications listen calls.
		 */
		this.applications.forEach((wrapper) => {
			const instanceWrapper = container.resolve(wrapper);
			instanceWrapper.listen();
		});

		this.crons.forEach((cronClass) => {
			const options = Reflect.getMetadata(CRON_META.OPTIONS, cronClass);
			const instance = container.resolve(cronClass);
			options.onTick = () => {
				if ('onTick' in instance) {
					instance.onTick();
				}
			};
			options.onComplete = () => {
				if ('onComplete' in instance) {
					instance.onComplete();
				}
			};
			instance.job = CronJob.from(options);
			if ('onInit' in instance) {
				instance.onInit();
			}
			this.log.logger.debug(`[cron-job][${cronClass.name}] registered.`);
		});
	}

}
