This library was generated with [Nx](https://nx.dev).
# Cli for  M backend projects

This project is a lightweight CLI mainly for generating & compiling [M Backend Core projects](https://www.npmjs.com/package/@m-backend/core)

## How to use
### Installation

Install the cli via npm with the following instructions:
```
npm i -g @m-backend/cli
```

> **Note:** We highly recommend installing the CLI globally in order to use it as it should be (especially for project generation).

### Usage

In order to execute anything, prefix the command with the keywords **m-cli**

Each command has its alternatives, feel free to use **one** of the possible alternatives for each command. 

| Command     | Arguments      | Options              | Default Value                                           | Possible values                 | Description                                                                                 |
|-------------|----------------|----------------------|---------------------------------------------------------|---------------------------------|---------------------------------------------------------------------------------------------|
| g, generate | {PROJECT_NAME} | -                    | -                                                       | demo                            | Generates a project                                                                         |
| r, run      | -              | -                    | -                                                       | -                               | Main command [run it with the documented options]                                           |
| r, run      | -              | -c, --config         | debug                                                   | production, dev or debug        | Config environment webpack                                                                  |
| r, run      | -              | -e, --entry          | ./src/main.ts                                           | -                               | Define the entry point                                                                      |
| r, run      | -              | --ormEntryPath       | ./src/app/database                                      |                                 | Define the entry path of the orm plugin                                                     |
| r, run      | -              | -o, --out            | dist                                                    | -                               | Defines the build output                                                                    |
| v, version  | -              | -                    | -                                                       | -                               | Give the version of the Cli                                                                 |
| ws, win-svc | -              | -                    | Autodetect name/project path                            | demo, C:\Dev                    | Convert a nodejs app to windows service ([lib](https://www.npmjs.com/package/node-windows)) |
| //          | -              | -i, --install        | install                                                 | -                               | Install windows service                                                                     |
| //          | -              | -u, --uninstall      | uninstall                                               | -                               | Uninstall windows service                                                                   |
| //          | -              | --max-old-space-size | -                                                       | 1536                            | Define the max old space size                                                               |
| //          | -              | --domain             | -                                                       | domain.local                    | Define domain (used by AD)                                                                  |
| //          | -              | --account            | -                                                       | demo                            | Define user account                                                                         |
| //          | -              | --password           | -                                                       | 1596150erofu                    | Define password                                                                             |
| //          | -              | --name               | Autodetect project name                                 | demo                            | Define custom project name                                                                  |
| //          | -              | --path               | Autodetect project path (remove project name and /dist) | C:\app\project                  | Define custom project path directory                                                        |
| //          | -              | --script             | Concat path + name + dist + app.js                      | C:\app\project\demo\dist\app.js | Define custom script directory                                                              |

### Examples

#### Generating a new project
```
m-cli g <YOUR_PROJECT_NAME> or m-cli generate <YOUR_PROJECT_NAME>
```
#### Compiling a project in debug mode
```
m-cli run or m-cli run --config={env}
```

#### Convert a project to Windows service
##### Install
```
m-cli ws -i or m-cli win-svc --install
```
###### With options
```
m-cli ws --name <PROJECT_NAME> --path <PATH> -i or m-cli win-svc --name demo --path C:\folders\ --install
```

##### Uninstall
```
m-cli ws -u or m-cli win-svc --uninstall
```

###### With options
```
m-cli ws --name <PROJECT_NAME> --path <PATH> -u or m-cli win-svc --name demo --path C:\folders\ --uninstall
```
