import WebpackShellPlugin from 'webpack-shell-plugin-next';
import devConfig from './dev';
import { Options } from '../cli.model';
import { Configuration } from 'webpack';
import { resolve } from 'path';

export default function (options: Options): Configuration {
  const config = devConfig(options);
  return {
    ...config,
    mode: 'development',
    watch: true,
    output: {
      ...config.output,
      clean: {
        keep: /node_modules/,
      },
      path: resolve(process.cwd(), options.out),
      globalObject: 'this',
    },
    plugins: [
      ...(config.plugins || []),
      new WebpackShellPlugin({
        onBuildEnd: {
          // TODO: use a m-cli command instead
          scripts: ['nodemon --quiet --inspect=127.0.0.1:9230 dist/app.js'],
          blocking: false,
          parallel: true,
        },
      }),
    ],
  };
}
