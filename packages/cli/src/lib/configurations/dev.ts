import { resolve } from 'path';
import prodConfig from './production';
import { Options } from '../cli.model';
import { Configuration } from 'webpack';

export default function (options: Options): Configuration {
  const config = prodConfig(options);
  return {
    ...config,
    mode: 'development',
    watch: false,
    devtool: 'inline-source-map',
    output: {
      ...config.output,
      path: resolve(process.cwd(), options.out),
    },
  };
}
