import { resolve, relative, posix } from 'path';
import { IgnorePlugin, Configuration, ResolvePluginInstance } from 'webpack';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import StatsPlugin from '../plugins/stats-plugin';
import { Options } from '../cli.model';
import webpackNodeExternals from 'webpack-node-externals';
import fs from 'fs';
import glob from 'glob';

const optionalPlugins: any[] = [];
if (process.platform !== 'darwin') {
  // don't ignore on OSX
  optionalPlugins.push(new IgnorePlugin({ resourceRegExp: /^fsevents$/ }));
}

export default function (options: Options): Configuration {
  // Use the specified ormEntryPath or a default value
  const databaseFolderPath = options.ormEntryPath || './src/app/database';
  let databaseFiles: any[] = [];
  if (fs.existsSync(databaseFolderPath)) {
    // Get all TypeScript files from the specified database folder path
    databaseFiles = glob.sync(`${databaseFolderPath}/**/*.ts`);
  }

  // Combine main entry point with database files
  const entry = {
    app: options.entry,
    ...databaseFiles.reduce((entries, file) => {
      const name = `database/${posix.relative(databaseFolderPath, file).replace('.ts', '')}`;
      entries[name] = file;
      return entries;
    }, {}),
  };

  return {
    mode: 'production',
    watch: false,
    stats: 'none',
    externalsPresets: {
      node: true,
    },
    performance: {
      hints: false,
    },
    optimization: {
      minimizer: [
        // Override the default TerserPlugin used by webpack for better debugging on production mode.
        new TerserPlugin({
          parallel: true,
          terserOptions: {
            keep_classnames: true,
          },
        }),
      ],
    },
    externals: [
      // Prevent *.config.json and config/*.js files from being included in the final app.js bundle.
      ({ context, request }: any, callback: any) => {
        if (/config\/.+/.test(request)) {
          callback(null, 'commonjs ' + request);
        } else {
          callback();
        }
      },
      // Prevent node modules bundling.
      webpackNodeExternals(),
    ],
    entry,
    output: {
      clean: true,
      filename: (pathData) => {
        // Output database files to dist/database and other files to dist
        const name = pathData.chunk?.name;
        return `[name].js`;
      },
      path: resolve(process.cwd(), options.out),
      globalObject: 'this',
    },
    resolve: {
      mainFields: ['main', 'module'],
      preferRelative: true,
      extensions: ['.ts', '.js'],
      plugins: [(new TsconfigPathsPlugin()) as ResolvePluginInstance], 
    },
    module: {
      rules: [
        {
          test: /node_modules[\/\\](iconv-lite)[\/\\].+/,
          resolve: {
            aliasFields: ['main'],
          },
        },
        {
          test: /\.ts$/,
          loader: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    plugins: [
      ...optionalPlugins,
      new CopyPlugin({
        patterns: [
          {
            from: 'config',
            to: 'config',
          },
          {
            from: 'data',
            to: 'data',
            noErrorOnMissing: true,
          },
          {
            from: 'www',
            to: 'www',
            noErrorOnMissing: true,
          },
          {
            from: 'package*.json',
            to: '[name][ext]',
          },
          {
            from: 'config/*.sql',
            to: 'config/[name][ext]',
            noErrorOnMissing: true,
          },
          {
            from: 'config/*.js',
            to: 'config/[name][ext]',
            noErrorOnMissing: true,
          },
          {
            from: 'src/batch_hash.js',
            to: '[name][ext]',
            noErrorOnMissing: true,
          },
          {
            from: '*.jar',
            to: '[name][ext]',
            noErrorOnMissing: true,
          },
          {
            from: 'keys',
            to: 'keys',
            noErrorOnMissing: true,
          },
          // Copy all files from the specified database folder, excluding .ts files
          ...(fs.existsSync(databaseFolderPath)
            ? [
              {
                from: `${databaseFolderPath}/`,
                to: 'database',
                noErrorOnMissing: true,
                globOptions: {
                  ignore: ['**/*.ts'], // Ignore .ts files
                },
              },
            ]
            : []),
        ],
      }),
      new StatsPlugin(),
    ],
  };
}
