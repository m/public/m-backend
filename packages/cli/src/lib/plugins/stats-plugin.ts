import clc from 'cli-color';
import table from 'text-table';
import { Compiler, WebpackError } from 'webpack';

export default class StatsPlugin {
	name = 'StatsPlugin';

    apply(compiler: Compiler) {
        const buildingMessageFormat = clc.bold.black.bgWhiteBright;
        console.log(buildingMessageFormat(' Webpack is building the project '));
		compiler.hooks.done.tap(this.name, (stats) => {
			const date = new Date();
			console.log(table([
				[clc.whiteBright('Built at:'), `${clc.cyan(date.toLocaleDateString())} ${clc.cyan(date.toLocaleTimeString())}`],
				[clc.whiteBright(`Time:`), `${clc.cyan(stats.endTime - stats.startTime)}ms`],
			]));

			const errors = stats.compilation.errors;
			const warnings = stats.compilation.warnings;
			const chunks = stats.compilation.chunks;
			errors.forEach(error => {
				console.log(this.errorOutput(error));
			});
			warnings.forEach(warning => {
				console.log(this.warningOutput(warning));
			});
			if (!errors.length && !warnings.length) {
				const renderedChunkFilenames = Array.from(chunks).filter(c => c.rendered).map(c => Array.from(c.files)[0]);
				const assets = Array.from(new Set(renderedChunkFilenames.concat(Array.from(stats.compilation.emittedAssets))));
				if (assets.length) {
					console.log(clc.whiteBright('Built Assets:'));
					console.log(table(assets.map((asset) => {
						return ['  ', clc.green(asset), this.formatSize(stats.compilation.assetsInfo.get(asset)?.size)];
					}), { align: ['l', 'r', 'l'] }))
					console.log(table([
						[clc.whiteBright('Output directory:'), `${clc.cyan(stats.compilation.compiler.outputPath)}`],
					]));
				} else {
					console.log(clc.whiteBright(`  ...Nothing to do`));
				}
			}
			console.log('\n');
		});
	}

	errorOutput(error: WebpackError) {
		console.log(error);
		// if (error.module)
		return clc.red(error.message);
		// if (error.error && error.error.formatted) {
		// 	return clc.red(error.error.formatted);
		// } else if (error.message && error.module && error.module.resource) {
		// 	return clc.yellow(error.module.resource) + "\n" + clc.red(error.message);
		// } else if (error.error && error.error.message) {
		// 	return clc.red(error.error.message);
		// } else if (error.error && error.error.error && error.error.error.formatted) {
		// 	return clc.red(error.error.error.formatted);
		// }
	}

	warningOutput(warnings: WebpackError) {
		return clc.yellow(warnings.message);
	}

	formatSize(size: number | undefined) {
		if (size !== 0 && !size) {
            return 'File size unavailable';
		}
		if (size <= 0) {
			return '0 bytes';
		}

		const abbreviations = ['bytes', 'kB', 'MB', 'GB'];
		const index = Math.floor(Math.log(size) / Math.log(1024));
		const roundedSize = size / Math.pow(1024, index);
		// bytes don't have a fraction
		const fractionDigits = index === 0 ? 0 : 2;

		return `${clc.yellow(roundedSize.toFixed(fractionDigits))} ${abbreviations[index]}`;
	}
}
