import {Service} from '@m-backend/core';
import {DataService, WorkerService} from '@shared/services';
import {IOEvents, WorkerActions} from '@shared/models';

@Service()
export class DataProcessService {
  public emitProvidedData: boolean = true;
    constructor(private dataService: DataService,private workerService: WorkerService) {
    }

    init() {
      this.dataService.ioEvents.on(IOEvents.RECEIVE_DATA, async ({type, version}) => {
        /**
         * Get consumed data with the following function
         * This consumed data is declared in the config file
         *
         * const data = this.dataService.getDataConsumed(<type>);
         */

        /**
         * In case you have a lot of data to process, you can use a worker with the following function
         * this.workerService.send({ emitProvidedData: true });
         */
        // Charge les données grâce au worker
        await this.workerService.send({ action: WorkerActions.PROCESS_DATA, emitProvidedData: this.emitProvidedData, dataConsumed: this.dataService.dataConsumed });
        this.emitProvidedData = false;
      });
    }
}
