import { Controller } from '@m-backend/core';
import MainApp from '@app/main.app';
import { IOEvents, ProvidedData, WorkerActions } from '@shared/models';
import { WorkerService } from '@shared/services/worker.service';
import { DataService } from '@shared/services';

@Controller()
export default class ReloadController {

	constructor (private app: MainApp,
		private workerService: WorkerService,
		private dataService: DataService) {
		this.listen();
	}

	listen(): void {
		this.dataService.ioEvents.on(IOEvents.RELOAD, (providedData: ProvidedData) => {
			this.workerService.send({action: WorkerActions.RELOAD, /*data*/ emitProvidedData: false });
		});
	}

}
