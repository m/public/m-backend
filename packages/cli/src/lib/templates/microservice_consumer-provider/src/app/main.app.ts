import { inject, injectAll } from "tsyringe";
import { config, AbstractApp, LoggerService, MiddlewareInterface, loadConfigFile, Application } from '@m-backend/core';
import { IOService } from "@shared/services";
import { DataProcessService } from "./services/data-process.service";


loadConfigFile('app');

@Application('main', config.app.port)
export default class MainApp extends AbstractApp {

	constructor (@inject(LoggerService) logger: LoggerService,
		@injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = [],
		private ioService : IOService,
		private processService: DataProcessService
		) {
		super(logger, middlewares);
    this.ioService.init();
    this.processService.init();
	}



}


