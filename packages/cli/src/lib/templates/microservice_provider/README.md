# Provider service

This is a service that identify as a data provider.

## Project 
This whole project is based on the M Backend Core/Cli libs and the M Commons git submodule:

 - [Core documentation](https://www.npmjs.com/package/@m-backend/core)
 - [Cli documentation (coming soon)](https://www.npmjs.com/package/@m-backend/cli)
 - [M Commons documentation](https://gitlab.mobilites-m.fr/m/microservices/m-commons)

You should at least reproduce the following configurations:
- Create an `app.config.json` file, we provide you with an `app.config.sample.json` as a basic example.
- Replace the `<PROJECT_NAME>` in package.json
