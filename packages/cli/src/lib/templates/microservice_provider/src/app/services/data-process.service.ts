import {Service} from '@m-backend/core';
import {DataService, WorkerService} from '@shared/services';
import {IOEvents, WorkerActions} from '@shared/models';

@Service()
export class DataProcessService {
  public emitProvidedData: boolean = true;
    constructor(private dataService: DataService,private workerService: WorkerService) {
    }

    async init() {

      // Charge les données grâce au worker
      await this.workerService.send({ action: WorkerActions.PROCESS_DATA, emitProvidedData: this.emitProvidedData, dataConsumed: this.dataService.dataConsumed });
      this.emitProvidedData = false;

    }
}
