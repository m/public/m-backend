import { Controller } from '@m-backend/core';
import { IOEvents, ProvidedData, WorkerActions } from '@shared/models';
import { DataService } from '@shared/services';
import { WorkerService } from '@shared/services/worker.service';

@Controller()
export default class ReloadController {

	constructor (
		private workerService: WorkerService,
		private dataService: DataService
	) {
		this.listen();
	}

	listen(): void {
		this.dataService.ioEvents.on(IOEvents.RELOAD, (providedData: ProvidedData) => {
			this.workerService.send({ action: WorkerActions.RELOAD, dataType: providedData.dataType, emitProvidedData: false });
		});
	}

}
