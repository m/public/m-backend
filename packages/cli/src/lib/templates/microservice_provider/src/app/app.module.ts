import { CoreModule, Module } from "@m-backend/core";
import MainApp from "./main.app";
import ReloadController from "./reload/reload.controller";
import LastUpdateController from "@shared/controllers/lastUpdate.controller";

@Module({
	imports: [CoreModule],
	applications: [MainApp],
	consoles: [],
	controllers: [ReloadController, LastUpdateController],
	crons: [],
	middlewares: []
})
export class AppModule { }
