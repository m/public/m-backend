import { inject, injectAll } from "tsyringe";
import { config, AbstractApp, LoggerService, MiddlewareInterface, loadConfigFile, Application } from '@m-backend/core';
import { IOService, WorkerService } from "@shared/services";
import { DataProcessService } from "./services/data-process.service";

/**
* The WorkerService and ioService are injected in the app's constructor for instantiation purposes.
* The WorkerService is possibly not needed depending on your service's logic, you can then remove it if it's your case
*/

loadConfigFile('app');

@Application('main', config.app.port)
export default class MainApp extends AbstractApp {

	constructor(
		// @ts-ignore
		@inject(LoggerService) logger: LoggerService,
		// @ts-ignore
		@injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = [],
		private workerService: WorkerService,
		private processService: DataProcessService,
		private ioService: IOService
	) {
		super(logger, middlewares);
		this.ioService.init();
		this.processService.init();

		/**
		 * In case you have a lot of data to process, you can use a worker with the following function
		 * this.workerService.send({ emitProvidedData: true });
		 */
	}
}
