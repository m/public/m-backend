import 'reflect-metadata';
import cluster from 'cluster';
import { bootstrap } from '@m-backend/core';
import { AppModule } from '@app/app.module';
import { App2Worker, AppActions, WorkerEvents } from '@shared/models';
import { formatReturnData, send2App } from '@shared/helpers';

if (cluster.isPrimary) {
	bootstrap(AppModule);
} else if (cluster.isWorker) {

	/**
	* This is your worker.
	* Worker logic should mainly focus on heavy CPU consuming tasks.
	* Note that NodeJS recommends to not handle file I/O in workers since Node natively takes care of it very well.
	**/

	process.on(WorkerEvents.MESSAGE, (event: App2Worker) => {
		try {
			/**
			* Implement your worker logic here.
			**/
			const dataTmp = formatReturnData('dataType','1.0.0',{ myNewData: 'Hello World !'});
            send2App({ action: AppActions.SEND_DATA, data: dataTmp, emitProvidedData: event.emitProvidedData });
        } catch (error: any) {
            send2App( { action: AppActions.SEND_ERROR, data: error.message });
        }
        send2App({ action: AppActions.PROCESS_ENDED });
	});


}
