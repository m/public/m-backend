import { CoreModule, Module } from "@m-backend/core";
import ConsumedDataController from "@shared/controllers/consumed-data.controller";
import MainApp from "./main.app";
import CorsMiddleware from "./middlewares/cors.middleware";
import OutputApp from "./output.app";
import OutputController from "./controllers/output.controller";

@Module({
	imports: [CoreModule],
	applications: [MainApp,OutputApp],
	consoles: [],
	controllers: [ConsumedDataController, OutputController],
	crons: [],
	middlewares: [CorsMiddleware]
})
export class AppModule { }
