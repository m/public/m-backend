import { CommuneProperties, GeoJSON } from "@shared/models";
import { Feature, FeatureCollection, Point, pointsWithinPolygon, Polygon, Properties } from "@turf/turf"
import { ParsedUrlQuery } from "querystring";

export function isInsideEpci(allEpci:any, properties: any, queryEpci:string) {
	if (queryEpci && queryEpci !== "None") {
		if (queryEpci === "All") return true;
		else if (!!properties.epci && queryEpci.indexOf(properties.epci) != -1) return true;//propriété epci
		else if (typeof (properties.epci) === 'undefined' && !Object.keys(allEpci).some(e => typeof (properties[e]) !== 'undefined')) return true; //aucune des propriétés "epci" n'existe : on retourne toujours tout
		else if (queryEpci.split(',').some(e => properties[e] === true)) return true;//une des propriétés epci correspond a une de celles de la requete
		else return false;
	}
	return true;
}

export function findObjectGeom(params: ParsedUrlQuery, gPoi: GeoJSON): GeoJSON {
	if (typeof (params.xmin) == 'undefined'
		|| typeof (params.xmax) == 'undefined'
		|| typeof (params.ymin) == 'undefined'
		|| typeof (params.ymax) == 'undefined') {

		return gPoi;
	}
	let searchWithin:Feature<Polygon> = {
            type: 'Feature',
            properties: {},
            geometry: {
                type: 'Polygon',
                coordinates: [[
                    [+params.xmin, +params.ymin],
                    [+params.xmin, +params.ymax],
                    [+params.xmax, +params.ymax],
                    [+params.xmax, +params.ymin],
                    [+params.xmin, +params.ymin]
                ]]
            }
	};

    let ptsWithin = pointsWithinPolygon(gPoi as FeatureCollection<Point,Properties>, searchWithin);
    return ptsWithin;

}