import { inject, injectAll } from "tsyringe";
import { config, AbstractApp, LoggerService, MiddlewareInterface, loadConfigFile, Application } from '@m-backend/core';

@Application('output', config.app.outputPort)
export default class OutputApp extends AbstractApp {
	constructor (
		@inject(LoggerService) logger: LoggerService,
		@injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = []
	) {
		super(logger, middlewares);
	}
}
