import { Service } from '@m-backend/core';
import Router from "@koa-better-modules/joi-router";
import { AppActions, gatewayRoutesSuffix, Worker2App } from '@shared/models';
import { DataService } from '@shared/services';

@Service()
export class RoutesService {

	public ownRoutes: Router.Spec[] = [];
	public publicRoutes: Router.Spec[] = [];
	public routes: Router.Spec[] = [];
	incomingTypes: string[] = []

	constructor (
		private dataService: DataService
	) { }

	addRoutes(routes: Router.Spec[], prefix: string) {
		//adding prefix
		let myRoutes: Router.Spec[] = JSON.parse(JSON.stringify(routes))
		myRoutes.forEach(r => r.path = `${prefix}${r.path}`);

		this.ownRoutes = this.ownRoutes.concat(myRoutes);

		let dataTmp: { [type: string]: { [version: string]: any } } = {};
		const type = `transit${gatewayRoutesSuffix}`;
		dataTmp[type] = { '1.0.0': this.ownRoutes };

        const event:Worker2App = {
            action: AppActions.SEND_DATA,
            data: dataTmp
        }

		this.dataService.copyDataTypes(event);
	}
}
