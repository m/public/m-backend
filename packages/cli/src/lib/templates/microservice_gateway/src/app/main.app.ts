import { inject, injectAll } from "tsyringe";
import { config, AbstractApp, LoggerService, MiddlewareInterface, loadConfigFile, Application } from '@m-backend/core';
import { DataService, IOService } from "@shared/services";


loadConfigFile('app');

@Application('main', config.app.inputPort)
export default class MainApp extends AbstractApp {

	constructor (
		@inject(LoggerService) logger: LoggerService,
		@injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = [],
		private ioService: IOService,
		private dataService: DataService,
	) {
		super(logger, middlewares);
		this.ioService.init();

		if (this.ioService.socket.connected) {
			this.ioService.emitNeedData(this.dataService.consumer);
		}
	}
}
