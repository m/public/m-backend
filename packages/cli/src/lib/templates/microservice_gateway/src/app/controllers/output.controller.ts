import 'reflect-metadata';
import { Context } from 'koa';
import { Controller, LoggerService, Get, AttachMiddleware, CONTROLLER_META } from '@m-backend/core';
import { DataService } from '@shared/services';
import { CommuneProperties } from '@shared/models';
import { cleanLib } from '../helpers/text.helper';
import { ParsedUrlQuery } from 'querystring';
import { isInsideEpci } from '../helpers/inside.helper';
import { epciSchema, querySchema, typeSchema } from '@shared/helpers';
import { RoutesService } from '../services/routes.service';
import CorsMiddleware from '../middlewares/cors.middleware';


@Controller({ app: 'output', prefix: '/api' })
export default class OutputController {
	constructor (
		private log: LoggerService,
		private dataService: DataService,
		private routesService: RoutesService,
	) {
		this.routesService.addRoutes(Reflect.getMetadata(CONTROLLER_META.ROUTES, this), Reflect.getMetadata(CONTROLLER_META.PREFIX, this.constructor));
	}

	@Get('/:type', {
		params: {
			type: typeSchema
		}
	},{
		description:'Tous les types qui entrent.',
		groupName: 'Debug',
		private: true
	})
	async getType(ctx: Context) {
		try{
			const type = ctx.request.params.type;
			ctx.response.body = this.dataService.getDataConsumed(type);
		} catch(e:any){
			this.log.logger.error(`[controller][OutputController][getType] ${e.message}`);
		}
	}

	// Exemple
	/* @AttachMiddleware([CorsMiddleware])
	@Get('/city/json', {
		query: {
			epci: epciSchema,
			query: querySchema
		}
	},{
		description:'Les communes sans leur geometrie.',
		groupName: 'Référentiel',
		private: false
	})
	async getCity(ctx: Context) {
		try{
			const params:ParsedUrlQuery = ctx.request.query;
			const type = 'communeProperties';
			const allEpci = this.dataService.getDataConsumed('epci');
			const filtered = this.dataService.getDataConsumed(type).filter(function (com:CommuneProperties) {
				if (!!params.epci && !isInsideEpci(allEpci, com, params.epci as string)) return false;
				if (!!params.query) {
					let query = cleanLib(params.query as string);
					let name = cleanLib(com.commune);
					if (name.indexOf(query) == -1) return false;
				}
				return true;
			})

			ctx.response.body = filtered;
		} catch(e:any){
			this.log.logger.error(`[controller][OutputController][getCity] ${e.message}`);
		}
	} */
}
