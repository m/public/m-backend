import { Feature, TypeDescription, GeoJSON } from "@shared/models";
import { Context } from "koa";
const j2xls = require('json2xls-xml')({ pretty: true });

export function cleanLib(lib: string): string {
	return lib.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[\-,\'']/g, ' ').replace(/[ ]+/g, ' ');
}

export function getLibelle(typeDescription: TypeDescription, feature: Feature):string {
	if (feature.properties.type == 'rue') return feature.properties.LIBELLE;
	else if (typeDescription && typeDescription.find) return feature.properties[typeDescription.find];
	else return '';
}
export function getCommune(typeDescription: TypeDescription, feature: Feature): string {
	if (feature.properties.type == 'clusters') return feature.properties.city;
	else if (typeDescription && typeDescription.findCommune) return feature.properties[typeDescription.findCommune];
	else return feature.properties.COMMUNE;
}
export function queryParamToArray(queryParam:string | string[] | undefined): string[] {
	let arrayQueryParam:string[] = [];
	if (!!queryParam) {
		if (Array.isArray(queryParam)) arrayQueryParam = queryParam;
		else arrayQueryParam = queryParam.split(',');
	}
	return arrayQueryParam;
}
export function formatXls(json: GeoJSON, ctx: Context) {
	let obj:any = { feuille1: [] };
	//object filling
	json.features.forEach(function (feature, index) {
		let properties = feature.properties;
		let geometry = feature.geometry;
		let xlsJsonLine:any = {};
		for (let p in properties) {
			xlsJsonLine[p] = properties[p];
		}
		xlsJsonLine.lon = geometry.coordinates[0];
		xlsJsonLine.lat = geometry.coordinates[1];
		obj.feuille1.push(xlsJsonLine);

	});
	//header's filling
	ctx.response.body = j2xls(obj);
	ctx.set('Content-Type', 'text/xls');
	ctx.set('Content-Disposition', 'attachment;filename=export.xls');
}
export function formatCsv(json: GeoJSON, ctx: Context) {
	let csvString = "";
	//initialisation of column's name
	let properties = json.features[0].properties;
	for (let p in properties) {
		csvString += "\"" + p + "\",";
	}
	csvString += "lon,";
	csvString += "lat";
	csvString += "\n";
	//column's filling
	json.features.forEach(function (feature, index) {
		let properties = feature.properties;
		for (let p in properties) {
			csvString += "\"" + properties[p] + "\",";
		}
		let geometry = feature.geometry;
		csvString += geometry.coordinates[0] + ",";
		csvString += geometry.coordinates[1] + "\n";
	});
	//header's filling
	ctx.response.body = csvString;
	ctx.set('Content-Type', 'text/csv');
	ctx.set('Content-Disposition', 'attachment;filename=export.csv');
}