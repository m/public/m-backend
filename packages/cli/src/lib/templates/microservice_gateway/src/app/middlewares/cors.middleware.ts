import { Context, Next } from 'koa';
import cors from '@koa/cors';
import { config, Middleware, MiddlewareInterface } from '@m-backend/core';

@Middleware()
export default class CorsMiddleware implements MiddlewareInterface {
	async use(ctx: Context, next: Next) {
		return await cors({
			origin: config.app.corsOrigin,
			allowMethods: 'GET,HEAD,PUT,POST,DELETE'
		})(ctx, next);
	}
}