import { Service } from '@m-backend/core';
import { EventEmitter } from 'events';

@Service()
export class LoremService {

	private readonly _eventEmitter: EventEmitter;
	get eventEmitter(): EventEmitter {
		return this._eventEmitter;
	}

	constructor () {
		this._eventEmitter = new EventEmitter();
	}
}
