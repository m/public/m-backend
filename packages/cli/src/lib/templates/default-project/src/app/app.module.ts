import { CoreModule, Module } from "@m-backend/core";
import LoremController from "@app/example-features/lorem.controller";
import MainApp from "./main.app";
import { LoremMiddleware } from "@app/example-features/lorem.middleware";
import LoremCron from "@app/example-features/lorem.cron";


/**
 * Do not forget to add your module dependencies in here.
 * All of them should be manually added to the AppModule.
 ************************************************************************************************
 *** You can remove then Lorem ones. They're intentionnally added for guidance purposes only. ***
 */

@Module({
	imports: [CoreModule],
	applications: [MainApp],
	consoles: [],
	controllers: [LoremController],
	crons: [LoremCron],
	middlewares: [LoremMiddleware]
})
export class AppModule { }
