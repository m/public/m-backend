import { Context } from 'koa';
import { Joi } from '@koa-better-modules/joi-router';
import { AttachMiddleware, Controller, LoggerService, Get, Post } from '@m-backend/core';
import { LoremMiddleware } from './lorem.middleware';


@Controller({ prefix: '/api' })
export default class LoremController {

	constructor (private log: LoggerService) { }

	// Middlewares can be used on a route level (example here) or on a higher level (controller, app). See docs for more.
	@AttachMiddleware([LoremMiddleware])
	@Get('/ipsum')
	async getIpsum(ctx: Context) {
		ctx.body = { message: 'test ipsum dolor sit amet' };
	}

	@Post('/lorem/:id', {
		body: {
			name: Joi.string().max(100).required(),
			email: Joi.string().lowercase().email().required()
		},
		type: 'json'
	})
	async postLorem(ctx: Context) {
		ctx.response.body = ctx.request.body;
		(ctx.response.body as any).id = ctx.request.params.id;
	}
}
