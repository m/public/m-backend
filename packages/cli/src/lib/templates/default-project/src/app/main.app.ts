import { inject, injectAll } from "tsyringe";
import { config, AbstractApp, LoggerService, MiddlewareInterface, loadConfigFile, Application } from '@m-backend/core';

loadConfigFile('app');

@Application('main', config.app.port)
export default class MainApp extends AbstractApp {

	constructor (@inject(LoggerService) logger: LoggerService,	@injectAll('MiddlewareInterface') middlewares: MiddlewareInterface[] = []) {
		super(logger, middlewares);
	}
}
