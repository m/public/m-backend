import { Context, Next } from "koa";
import { Middleware, MiddlewareInterface } from "@m-backend/core";

@Middleware()
export class LoremMiddleware implements MiddlewareInterface {
	async use(ctx: Context, next: Next) {
		return await next()
	}
}
