import 'reflect-metadata';
import { bootstrap } from '@m-backend/core';
import { AppModule } from '@app/app.module';

bootstrap(AppModule);
