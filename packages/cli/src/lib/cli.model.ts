import {Service} from 'node-windows';


export interface Options {
  config: string;
  entry: string;
  generate: string;
  ormEntryPath: string;
  out: string;
  watch: boolean;
  version: string;
  windowsNode: {
    service: string;
    run: string;
    action: string;
  };
}

export interface WebpackConfig {
  config: string;
  entry?: string;
  out?: string;
  orm_entry_path?: string;
  show?: boolean;
}


export interface TemplateData {
  projectName: string
}

export interface CliOptions {
  projectName: string
  templateName: string
  templatePath: string
  targetPath: string
}

export enum ActionWS {
  INSTALL = 'install',
  UNINSTALL = 'uninstall'
}

export interface NodeOptionsWS {
  name?: string;
  description?: string;
  path?: string;
  directory?: string;
  script?: string;
  maxOldSpaceSize?: number;
  domain?: string;
  account?: string;
  password?: string;
}

export interface ServiceWindows extends Service{
  _directory?: string;
  allowServiceLogon?: boolean;
}
