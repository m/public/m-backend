import chalk from 'chalk';
import * as ejs from 'ejs';
import * as fs from 'fs';
import inquirer from 'inquirer';
import * as path from 'path';
import {resolve} from 'path';
import * as shell from 'shelljs';
import {webpack} from 'webpack';
import { ActionWS, CliOptions, NodeOptionsWS, ServiceWindows, TemplateData, WebpackConfig } from './cli.model';
import {debugConfig, devConfig, prodConfig} from './configurations';

// Node Windows
import {EventLogger, Service} from 'node-windows';

const CHOICES: string[] = fs.readdirSync(path.join(__dirname, 'templates'));
const QUESTIONS: any = [
  {
    name: 'template',
    type: 'list',
    message: 'Quel type de projet souhaitez-vous générer ?',
    choices: CHOICES,
  }
];

const CURR_DIR: string = process.cwd();
const SKIP_FILES: string[] = ['node_modules', '.template.json'];

const configurations: any = {
  production: prodConfig.default,
  dev: devConfig.default,
  debug: debugConfig.default,
};


/**
 * Configure webpack builder
 * @param options WebpackConfig
 * @returns Promise<void>
 */
export async function builderConfig(options: WebpackConfig): Promise<void> {
  let webpackConfig = configurations[options.config] || null;

  if (!webpackConfig) {
    const module = await import(resolve(process.cwd(), options.config));
    webpackConfig = module.default;
  }
  const context = {
    configurations,
  };
  const compiler = webpack(webpackConfig(options, context));
  compiler.run((err) => {
    if (err) console.log(chalk.red(err));
  });
}


/**
 * Use for generate a new project
 * @param projectName
 */
export function generateProject(projectName: string): void {
  inquirer.prompt(QUESTIONS).then(answers => {
    const projectChoice = answers['template'];
    const templatePath: string = path.join(__dirname, 'templates', projectChoice);
    const targetPath: string = path.join(CURR_DIR, projectName);

    const options: CliOptions = {
      projectName, templateName: projectChoice, templatePath, targetPath: targetPath,
    };

    if (!createProject(targetPath)) {
      return;
    }

    createDirectoryContents(templatePath, projectName);

    postProcess(options);
  });
}

/**
 * Create a new project folder
 * @param projectPath string
 * @returns boolean
 */
function createProject(projectPath: string): boolean {
  if (fs.existsSync(projectPath)) {
    console.log(chalk.red(`Folder ${projectPath} exists. Delete or use another name.`));
    return false;
  }
  fs.mkdirSync(projectPath);

  return true;
}


/**
 * Creates a hierarchy of files and folders in a destination directory from a template located at the specified path.
 * @param templatePath - The path to the file/folder template.
 * @param projectName - The project name to customize the template content.
 */

function createDirectoryContents(templatePath: string, projectName: string): void {
  // read all files/folders (1 level) from template folder
  const filesToCreate = fs.readdirSync(templatePath);
  // loop each file/folder
  filesToCreate.forEach(file => {
    const origFilePath = path.join(templatePath, file);

    // get stats about the current file
    const stats = fs.statSync(origFilePath);

    // skip files that should not be copied
    if (SKIP_FILES.indexOf(file) > -1) return;

    if (stats.isFile()) {
      // read file content and transform it using template engine
      let contents = fs.readFileSync(origFilePath, 'utf8');
      contents = render(contents, {projectName});
      // write file to destination folder
      const writePath = path.join(CURR_DIR, projectName, file);
      fs.writeFileSync(writePath, contents, 'utf8');
    } else if (stats.isDirectory()) {
      // create folder in destination folder
      fs.mkdirSync(path.join(CURR_DIR, projectName, file));
      // copy files/folder inside current folder recursively
      createDirectoryContents(path.join(templatePath, file), path.join(projectName, file));
    }
  });
}

/**
 * Add git init and install dependencies
 * @param options CliOptions
 * @returns boolean
 */
function postProcess(options: CliOptions): boolean {

  const isNode = fs.existsSync(path.join(options.templatePath, 'package.json'));
  let gitSubmoduleResult;
  if (isNode) {
    shell.cd(options.targetPath);
    const gitInitResult = shell.exec('git init');
    console.info('Generating project...');
    // If it is a microservice
    if (options.templateName.includes('microservice')) {
      console.info('Adding git submodule...');
      gitSubmoduleResult = shell.exec('git submodule add https://gitlab.mobilites-m.fr/m/microservices/m-commons.git');
    }
    console.info('Installing dependencies...');
    const npmResult = shell.exec('npm install');
    if (npmResult.code !== 0 || (gitSubmoduleResult && gitSubmoduleResult.code !== 0) || gitInitResult.code !== 0) {
      return false;
    }
  }
  return true;
}

/**
 * Render template
 * @param content string
 * @param data TemplateData
 * @returns string
 */
function render(content: string, data: TemplateData): string {
  return ejs.render(content, data);
}

/**
 * Use for clean param
 * @Example
 * paramCleaner('=debug', '=') => 'debug'
 * @param str
 * @param symbol
 * @returns string
 */
export function paramCleaner(str: string, symbol: string): string {
  if (str.includes(symbol)) return str.replace(symbol, ''); else return str;
}


/**
 * Use lib node-windows for create a Windows service
 * @param name project name (Must be identical to the project name)
 * @param path path to the node project: C:/microservices/${name}
 * @param action install or uninstall
 * @param params node [options](https://nodejs.org/api/cli.html#cli_node_options)
 */
export function convertNodeToService(name: string, path: string, action: ActionWS | null, params: NodeOptionsWS): void {
  // Path: project path without the project name
  // Directory: path where the service's {deamon} folder will be installed
  // Name: name of the service
  // Description: description of the service
  // Script: path to the service's script
  // NodeOptions: node options

  if (action === null) throw new Error('You must specify an action: install or uninstall');

  // Check if we use the name option
  if (params.name) {
    if (!params.path) throw new Error('You must specify a custom path with options --name');
    name = params.name;
  }

  if (params.path) {
    if (!params.name) throw new Error('You must specify a custom name with options --path');
    path = params.path || path;
  }

  // Default path
  const description = params.description || `Service generated by @m-backend/cli for ${name}`;
  // Script path
  const script = params.script ? params.script : `${path}\\${name}\\dist\\app.js`;

  const Log: EventLogger = new EventLogger({
    source: `M:${name}`,
    eventLog: 'APPLICATION',
  });
  const nodeOptions: string[] = []
  if (params.maxOldSpaceSize) nodeOptions.push(`--max-old-space-size=${params.maxOldSpaceSize}`);

  const svc: ServiceWindows = new Service({
    name,
    description,
    script,
    nodeOptions,
  });
  // Used for install Deamon folder in correct path
  svc._directory = params.directory || `${path}\\${name}`;

  if (params.domain) svc.logOnAs.domain = params.domain;
  if (params.account) svc.logOnAs.account = params.account;
  if (params.password) svc.logOnAs.password = params.password;

  const isExists: boolean = svc.exists;

  if (isExists && action === ActionWS.INSTALL) {
    console.error(`Service ${name} already exists, you must uninstall it first if you want to reinstall it.`);
    return;
  }

  if (action === ActionWS.INSTALL) {
    // If the service does not exist and the action is install: install the service
    svc.on('install', () => {
      console.info(`Install complete.`);
      Log.info('Install complete.');
      svc.start();
    });

    svc.on('alreadyinstalled', () => {
      console.warn(`Service ${name} already installed.`);
      Log.warn('Already installed.');
    });

    svc.on('invalidinstallation', () => {
      console.warn(`Service ${name} invalid installation.`);
      Log.warn('Invalid installation.');
    });

    svc.on('error', () => {
      console.error(`Service ${name} error`);
      Log.error('Error.');
    });

    svc.install();
  } else if (isExists && action === ActionWS.UNINSTALL) {
    console.info(`Service ${name} uninstalling ...`);
    // If the service exists and the action is uninstall: uninstall the service
    svc.on('uninstall', () => {
      console.info(`Uninstall complete.`)
      Log.info('Uninstall complete.');
    });

    svc.uninstall();
  } else {
    console.error(`Service ${name} does not exist, you must install it first if you want to uninstall it.`);
  }


}
