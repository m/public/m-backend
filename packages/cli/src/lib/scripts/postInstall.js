#!/usr/bin/env node
// This piece of code is based on Facebook's official github React repository
// It's available here: https://github.com/facebook/create-react-app/blob/2da5517689b7510ff8d8b0148ce372782cb285d7/packages/react-scripts/scripts/init.js#L264-L278
// It corrects a well known issue: npm not including .gitignore files into published packages.
// See: https://github.com/npm/npm/issues/1862 && https://github.com/npm/npm/issues/3763

const fs = require('fs');
const path = require('path');

const templatesDirectory = path.join(__dirname, '../templates');

fs.readdirSync(templatesDirectory, {
  withFileTypes: true,
})
  .filter((c) => c.isDirectory())
  .map((c) => c.name)
  .forEach((templateName) => {
    // This is the copied base React code
    const appPath = path.join(templatesDirectory, templateName);
    const gitignoreExists = fs.existsSync(path.join(appPath, '.gitignore.txt'));
    if (gitignoreExists) {
      // Append if there's already a `.gitignore` file there

      fs.renameSync(path.join(appPath, '.gitignore.txt'), path.join(appPath, '.gitignore'));
    } 

  });
