#!/usr/bin/env node
import {Command} from 'commander';
import {version} from '../package.json';
import {ActionWS, NodeOptionsWS, WebpackConfig} from '../src/lib/cli.model';
import {Cli} from '../src'
// Commander
const program: Command = new Command();

// Version CLI
program.command('version').alias('v')
  .description('Check current m-cli version')
  .action(() => {
    console.log(`@m-backend/cli version: ${version}`);
  })

// Setup webpack configuration
program.command('run').alias('r')
  .description('Main command [run it with the documented options]')
  .option('-c, --config <config>', 'Custom webpack configuration', 'debug')
  .option('-e, --entry <entry>', 'Define the entry point', './src/main.ts')
  .option('--ormEntryPath, [orm_entry_path]', 'Define the entry path of the orm plugin', './src/app/database')
  .option('-o, --out <out>', 'Define the build output', 'dist')
  .action((options: WebpackConfig): void => {
    if (options.config) options.config = Cli.paramCleaner(options.config, '=');
    if (options.entry) options.entry = Cli.paramCleaner(options.entry, '=');
    if (options.out) options.out = Cli.paramCleaner(options.out, '=');
    if (options.orm_entry_path) options.orm_entry_path = Cli.paramCleaner(options.orm_entry_path, '=');
    Cli.builderConfig(options);
  })

// Create a new project
program.command('generate').alias('g')
  .description('Create a new project')
  .argument('<project_name>', 'Name of the project')
  .action((projectName) => {
    if (!projectName) throw new Error('You must specify a project name');
    else Cli.generateProject(projectName);
  })

// Compile node project for use in Windows service
program.command('win-svc').alias('ws')
  .option('-i, --install', 'Install the service', 'install')
  .option('-u, --uninstall', 'Uninstall the service', 'uninstall')
  .option('--max-old-space-size <size>', 'Define the max old space size')
  .option('--domain <domain>', 'Define domain: [default --> "NT AUTHORITY"] ')
  .option('--account <account>', 'Define account: [default --> "LocalSystem"]')
  .option('--password <password>', 'Define password: [default --> ""]')
  .option('--description <description>', 'Define custom description')
  .option('--name <name>', 'Define custom service name, if we use it need to add custom path')
  .option('--path <path>', 'Define custom path')
  .option('--script <script>', 'Define custom script path')
  .option('--directory <directory>', 'Define custom directory path with install service folder')
  .description('Compile node project for use in windows service')
  .action((options) => {
    const params: NodeOptionsWS = {...options};
    if (options && (options.install === true || options.uninstall === true)) {
      let action = null;
      if (options.install === true) action = ActionWS.INSTALL;
      if (options.uninstall === true) action = ActionWS.UNINSTALL;
      if (options.maxOldSpaceSize && !isNaN(options.maxOldSpaceSize)) params.maxOldSpaceSize = parseInt(options.maxOldSpaceSize);

      const absolutePath = process.cwd();
      const parts = absolutePath.split(/[/\\]/);
      const serviceName = parts[parts.length - 2] || 'defaultName';
      const customPath = absolutePath.replace(`\\${serviceName}\\dist`, '');

      Cli.convertNodeToService(serviceName, customPath, action, params);
    } else {
      throw new Error('You must specify only one option: -i or -u');
    }

  })


program.parse(process.argv);
